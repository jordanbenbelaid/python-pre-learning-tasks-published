def vowel_swapper(string):

    # ==============
    # Your code here

    for r in (("a", "4"), ("A", "4")):
        string = string.replace(*r)

    for r in (("e", "3"), ("E", "3")):
        string = string.replace(*r)

    for r in (("i", "!"), ("I", "!")):
        string = string.replace(*r)

    for r in (("o", "ooo"), ("O", "000")):
        string = string.replace(*r)

    for r in (("u", "|_|"), ("U", "|_|")):
        string = string.replace(*r)

    return string
    # ==============


print(vowel_swapper("aA eE iI oO uU"))  # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World"))  # Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
