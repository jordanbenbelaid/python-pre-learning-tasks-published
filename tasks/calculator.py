def calculator(a, b, operator):
    # ==============
    # Your code here

    #addition
    if operator == '+':
        return a+b

    #subtraction
    if operator == '-':
        return a-b
    #division
    if operator == '/':
        return a/b

    #multiplication
    if operator == '*':
        return a*b

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
