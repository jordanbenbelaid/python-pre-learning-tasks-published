def vowel_swapper(string):
    # ==============
    # Your code here
    string = replace2(string, "a", "A", "/\\")
    string = replace2(string, 'e', 'E', '3')
    string = replace2(string, 'i', 'I', '!')
    string = replace2(string, 'u', 'U', '\\/')

    for i in range(0, len(string)):
        if string[i] == "o" or string[i] == "O":
            for j in range(i + 1, len(string)):
                if string[j] == "o":
                    string = string[:j] + ('ooo') + string[j + 1:]
                    return string

                if string[j] == "O":
                    string = string[:j] + ('000') + string[j + 1:]
                    return string

    return string


def replace2(string, up, low, change):
    for i in range(0, len(string)):
        if string[i] == low or string[i] == up:
            for j in range(i + 1, len(string)):
                if string[j] == low or string[j] == up:
                    string = string[:j] + change + string[j + 1:]
                    return string
    return string


# ==============

print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
