def calculator(a, b, operator):
    # ==============
    # Your code here

    # addition
    if operator == '+':
        return bin(a + b)[2:]

    # subtraction
    if operator == '-':
        return bin(a - b)[2:]
    # division
    if operator == '/':
        return bin(int(a / b))[2:]

    # multiplication
    if operator == '*':
        return bin(a * b)[2:]

    # ==============


print(calculator(2, 4, "+"))  # Should print 110 to the console
print(calculator(10, 3, "-"))  # Should print 111 to the console
print(calculator(4, 7, "*"))  # Should output 11100 to the console
print(calculator(100, 2, "/"))  # Should print 110010 to the console
